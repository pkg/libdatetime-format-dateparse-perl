libdatetime-format-dateparse-perl (0.05-2.1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 22 Apr 2021 15:20:33 +0200

libdatetime-format-dateparse-perl (0.05-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 29 Dec 2020 00:48:02 +0100

libdatetime-format-dateparse-perl (0.05-2) unstable; urgency=low

  [ gregor herrmann ]
  * Remove alternative (build) dependencies that are already satisfied
    in oldstable.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Bump debhelper compatibility level to 9.
  * Add build dependency on libmodule-build-perl.
  * Update years of packaging copyright.
  * Update license stanzas in debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 05 Jun 2015 16:52:47 +0200

libdatetime-format-dateparse-perl (0.05-1) unstable; urgency=low

  * New upstream release.
  * Set Standards-Version to 3.8.4 (no changes).
  * Remove patch nanoseconds.patch (fixed upstream) and quilt framework.
  * Convert to source format 3.0 (quilt).
  * debian/copyright: update formatting.
  * debian/control: remove versions from libdatetime-timezone-perl,
    libdatetime-perl, libtimedate-perl (build) dependency, all already fulfilled
    in oldstable.

 -- gregor herrmann <gregoa@debian.org>  Fri, 16 Apr 2010 22:14:06 +0200

libdatetime-format-dateparse-perl (0.04-2) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ryan Niebur ]
  * Update jawnsy's email address
  * Update ryan52's email address

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.
  * Add patch nanoseconds.patch to handle newer DateTime behaviour regarding
    nanoseconds; add quilt framework (closes: #560637).
  * Set Standards-Version to 3.8.3 (no changes).
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Dec 2009 16:23:47 +0100

libdatetime-format-dateparse-perl (0.04-1) unstable; urgency=low

  * Initial Release. (Closes: #529547)

 -- Ryan Niebur <ryanryan52@gmail.com>  Sat, 23 May 2009 01:08:52 -0700
